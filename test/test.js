var assert = require('assert');
var javascriptFunctions = require('../javascriptFunctions');
var OctalConverter = require('../OctalConverter');

describe('givenTwoIntegersWhenSumThenSucess()', function (){
    it('sum(1, 2) = 3', function(){
        //Given
        var a = 1;
        var b = 2;
        var expectedResult = 3;

        //When
        var actualResult = javascriptFunctions.sum(a, b);

        //Then
        assert.equal(expectedResult, actualResult);
    });
});

describe ('givenDecimalWhenConvertThenOctal"()', function(){
    it('1 octal for 1 in decimal', function(){
        //Given
         c = 1;
        var expectedResult = 1;

        //When
        var actualResult = OctalConverter.convert(c);

        //Then
        assert.equal(expectedResult, actualResult);
    });
});

describe ('givenDecimalWhenConvertThenOctal"()', function(){
    it('2 octal for 2 in decimal', function(){
        //Given
         c = 2;
        var expectedResult = 2;

        //When
        var actualResult = OctalConverter.convert(c);

        //Then
        assert.equal(expectedResult, actualResult);
    });
});

describe ('givenDecimalWhenConvertThenOctal"()', function(){
    it('10 octal for 8 in decimal', function(){
        //Given
         c = 8;
        var expectedResult = 10;

        //When
        var actualResult = OctalConverter.convert(c);

        //Then
        assert.equal(expectedResult, actualResult);
    });
});

describe ('givenDecimalWhenConvertThenOctal"()', function(){
    it('137 octal for 95 in decimal', function(){
        //Given
         c = 95;
        var expectedResult = 137;

        //When
        var actualResult = OctalConverter.convert(c);

        //Then
        assert.equal(expectedResult, actualResult);
    });
});

describe ('givenDecimalWhenConvertThenOctal"()', function(){
    it('4000 octal for 2048 in decimal', function(){
        //Given
         c = 2048;
        var expectedResult = 4000;

        //When
        var actualResult = OctalConverter.convert(c);

        //Then
        assert.equal(expectedResult, actualResult);
    });
});