function convert(c){
    if(c < 8){
        return c;
    }else{
        return (c).toString(8);
    }
}

exports.convert = convert;